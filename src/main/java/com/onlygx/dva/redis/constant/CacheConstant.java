package com.onlygx.dva.redis.constant;

/**
 * <p>
 * 描述 : redis Cache 相关命名 名称和配置的关系参考 RedisCacheManagerConfig 类
 * </p>
 *
 * @author GaoXiang
 * @version V1.1
 * @since 19-1-10
 */
public class CacheConstant {

    //header中用户参数
    public static final String CURRENT_USER = "current-user";

    // 默认的cache ,缓存保留时间 60*12 分钟
    public static final String CACHE = "cache";

    // 1分钟的cache ,缓存保留时间 1 分钟
    public static final String CACHE_1M = "cache1M";

    // 5分钟的cache ,缓存保留时间 5 分钟
    public static final String CACHE_5M = "cache5M";

    // 10分钟的cache ,缓存保留时间 10 分钟
    public static final String CACHE_10M = "cache10M";

    // 30分钟的cache ,缓存保留时间 30 分钟
    public static final String CACHE_30M = "cache30M";

    // 60分钟的cache ,缓存保留时间 60 分钟
    public static final String CACHE_60M = "cache60M";

    // 60分钟的cache ,缓存保留时间 60 分钟
    public static final String CACHE_SHIRO = "cacheShiro";

    /**
     * Spring 使用CGLIB后 类对象命名会加上的内容字符串
     */
    public static final String SPRING_CGLIB_SUFFIX = "$$EnhancerBySpringCGLIB$$";

    /**
     * 缓存key生成拼接时用的分隔符
     */
    public static final String CACHE_KEY_SEPARATOR = ":";

    public static final String PHONE_CODE = "ip:core:phone:code:";


}

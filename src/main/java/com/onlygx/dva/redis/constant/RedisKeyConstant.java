package com.onlygx.dva.redis.constant;

/**
 * 高祥
 * 2023年7月28日11:10:25
**/
public class RedisKeyConstant {


    //隔断符号
    public static final String KEY_SEPARATOR = ":";

    //前缀
    public static final String KEY_PREFIX = "ip" + KEY_SEPARATOR + "core" + KEY_SEPARATOR;

    //重复提交
    public static final String METHOD_REPEAT_SUBMIT = KEY_PREFIX + "method:repeat:submit:";

    public static final String EVENT_REDIS_KEY_START = KEY_PREFIX + "event:push:";

    public static final String DEPLOY_CHECK_FAIL = KEY_PREFIX + "check:fail:";


}

package com.onlygx.dva.redis.exception;

/**
 * <p>
 * 重复提交异常
 * </p>
 */
public class RepeatRequestException extends RuntimeException {

    private static final String DEFAULT_MESSAGE = "操作失败 - 请勿重复提交。";

    public RepeatRequestException(String message) {
        super(message);
    }

    public RepeatRequestException() {
        super(DEFAULT_MESSAGE);
    }
}

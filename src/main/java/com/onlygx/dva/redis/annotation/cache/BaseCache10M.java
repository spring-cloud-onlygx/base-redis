package com.onlygx.dva.redis.annotation.cache;


import com.onlygx.dva.redis.constant.CacheConstant;
import org.springframework.cache.annotation.Cacheable;

import java.lang.annotation.*;

/**
 * 描述 : 自定义注解 缓存时间10分钟
 * @author GaoXiang
 * @version V1.1
 * @since 19-1-10
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
@Cacheable(value = CacheConstant.CACHE_10M)
public @interface BaseCache10M {

}

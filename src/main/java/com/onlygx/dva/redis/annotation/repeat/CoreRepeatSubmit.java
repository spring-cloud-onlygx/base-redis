package com.onlygx.dva.redis.annotation.repeat;

import java.lang.annotation.*;

/**
 * <p>
 * 避免重复提交
 * </p>
 *
 * @author 高祥
 * @since 2023-03-13
 */

@Inherited
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CoreRepeatSubmit {

    /**
     * 防重复操作限时标记数值(存储redis限时标记数值)
     */
    String value() default "value" ;

    /**
     * 防重复操作过期时间(借助redis实现限时控制)
     */
    long expireSeconds() default 5;

    /**
     * 初始化的Key是否包含用户信息
     * 2023年3月14日11:11:52 高祥
     */
    boolean includeUser() default true;
}
